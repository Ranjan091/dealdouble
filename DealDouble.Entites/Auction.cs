﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDouble.Entites
{
   public  class Auction
    {
        public int ID { get; set; }
        public string Title { get; set; }
       // public string PictureUrl { get; set; }
        public string Description { get; set; }
        public decimal ActualAmount{ get; set; }
        public DateTime StratingTime { get; set; }
        public DateTime EndingDate { get; set; }
    }
}

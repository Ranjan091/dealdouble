﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DealDouble.Web.ViewModel
{
    public class PageViewModel
    {
        public string PageTitel{get;set;}
        public string PageDescription { get; set; }
    }
}
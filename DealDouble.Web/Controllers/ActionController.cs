﻿using DealDouble.Entites;
using DealDouble.services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDouble.Web.Controllers
{
    public class ActionController : Controller
    {
        auctionservice auctionservice = new auctionservice();
        [HttpGet]
        public ActionResult index()
        {
            var auctions = auctionservice.getallauction();
            if (Request.IsAjaxRequest())
            {
                return PartialView(auctions);
            }
            else
            {
                return View(auctions);
            }


        }

        // GET: Action
        public ActionResult Create()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(Auction auction)
        {
            
            auctionservice.saveauction(auction);
            return RedirectToAction("index");
        }
        [HttpGet]
        public ActionResult Edit(int ID)
        {
      
            var action = auctionservice.getauctionByid(ID);
            return PartialView(action);
        }
        [HttpPost]
        public ActionResult Edit(Auction auction)
        {
            auctionservice.Updateauction(auction);
            return RedirectToAction("index");
            //return View(auction);
        }
        [HttpGet]
        public ActionResult delete(int ID)
        {
            
           var auction= auctionservice.getauctionByid(ID);

            return View(auction);
        }
        [HttpPost]
        public ActionResult delete(Auction auction)
        {
           
            auctionservice.deleteauction(auction);

            return RedirectToAction("index");

        }
        [HttpGet]
        public ActionResult details(int ID)
        {
       
            var auction = auctionservice.getauctionByid(ID);

            return View(auction);
        }




    }
}
﻿using DealDouble.services;
using DealDouble.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DealDouble.Web.Controllers
{
    public class HomeController : Controller
    {
        auctionservice auctionservices = new auctionservice();
        public ActionResult Index()
        {
            ActionsViewModel actionsView = new ActionsViewModel();
           actionsView.allauctions = auctionservices.getallauction();
          actionsView.promotedauction= auctionservices.getapropomtedauction();
           // ViewBag.Title = "dealdash";
            actionsView.PageTitel = "deal dash";
            actionsView.PageDescription = "hello it is adeal dash program";
            return View(actionsView);
        }
       


        public ActionResult About()
        {

            ViewBag.Message = "Your  description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
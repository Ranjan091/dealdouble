﻿using DealDouble.Data;
using System;
using DealDouble.Entites;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDouble.services
{
   public  class auctionservice
    {
        DealDoubleContext context = new DealDoubleContext();

        public List<Auction> getallauction()
        {

            return context.auctions.ToList();

        }
        public List<Auction> getapropomtedauction()
        {

            return context.auctions.Take(3).ToList();

        }


        public Auction getauctionByid(int ID)
        {
      
            return context.auctions.Find(ID);
         
        }
        public void saveauction(Auction auction)
        {
            context.auctions.Add(auction);
            context.SaveChanges();
        }
        public void Updateauction(Auction auction)
        {
            context.Entry(auction).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }
        public void deleteauction(Auction auction)
        {
            context.Entry(auction).State = System.Data.Entity.EntityState.Deleted;
            context.SaveChanges();
        }


    }
}

﻿using DealDouble.Entites;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealDouble.Data
{
    public class DealDoubleContext:DbContext
    {
        public DealDoubleContext():base("name=DealDoubleContext")
            {

            }
        public DbSet<Auction> auctions { get; set; }
    }
}
